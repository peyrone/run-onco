#!/usr/bin/env python3

'''
Author          : Neda Peyrone
Create Date     : 19-09-2019
File            : app_api.py
Purpose         : -
'''
import logging

from flask import Blueprint, request, jsonify

from api.constants import app_constant as const
from api.utils import app_util

logger = logging.getLogger(__name__)

app_api = Blueprint('app_api', __name__)


@app_api.route('/executeJob', methods=['POST'])
def execute_job():
    logger.info('I:--START--:--Execute Job--')

    try:
        req = request.get_json()
        # s = 'cd {} msisensor msi -d example.microsate.sites -n example.normal.bam -t example.tumor.bam -e example.bed -o example.paired.output -l 1 -q 1 -b 2'.format(const.INCOMING_DIR)
        command = "msisensor msi " \
                  "-d {0}/example.microsate.sites " \
                  "-n {0}/example.normal.bam " \
                  "-t {0}/example.tumor.bam " \
                  "-e {0}/example.bed " \
                  "-o {1}/example.paired.output " \
                  "-l 1 -q 1 -b 2".format(const.INCOMING_DIR, const.OUTGOING_DIR)
        # (out, errors) = app_util.run_cmd(command.split())
        # s = 'cd {} '\
        #     'ls -l'.format(const.INCOMING_DIR)
        logger.info("O:--PrintOut Command--:cmd/{}".format(command))
        arr = command.split()
        # print(arr)
        (out, errors) = app_util.run_cmd(arr)
        # print(out)
        logger.info('O:--SUCCESS--:--Execute Job--')
    except Exception as e:
        logger.info('O:--FAIL--:--Execute Job--:errorDesc/{}'.format(e))
        # logging.fatal(e.message)

    return jsonify({}), 200
